import { Component } from "@angular/core";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  public visible: boolean = false;

  constructor() {}

  public onClickHandle(): void {
    this.visible = !this.visible;
  }
}
