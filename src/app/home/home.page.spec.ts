import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';
import { By } from '@angular/platform-browser';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();

    const button = fixture.debugElement.nativeElement.querySelector('#button')
    const text = fixture.debugElement.nativeElement.querySelector('#text')

    expect(button.textContent).toContain('Show')
    expect(text).toBeNull()

    button.click()
    fixture.detectChanges();
    const textShow = fixture.debugElement.nativeElement.querySelector('#text')

    expect(button.textContent).toContain('Hide')
    expect(textShow.textContent).toContain('Showing text')
  });
});
